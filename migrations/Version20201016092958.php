<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201016092958 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE camping (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, users_id INTEGER NOT NULL, logement CLOB NOT NULL, description CLOB NOT NULL, type INTEGER NOT NULL, prix INTEGER NOT NULL, taille CLOB NOT NULL, image VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_81A904E467B3B43D ON camping (users_id)');
        $this->addSql('CREATE TABLE factures (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, date_debut DATE NOT NULL, date_fin DATE NOT NULL, logement CLOB NOT NULL, prix INTEGER NOT NULL, taxe INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE ligne_facture (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, quantiter INTEGER NOT NULL, logement CLOB NOT NULL, prix_deduit INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE planing (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, saison INTEGER NOT NULL, date DATE NOT NULL)');
        $this->addSql('CREATE TABLE profils (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, prenom VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE taxe_piscine (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, prix_enfant INTEGER NOT NULL, prix_adulte INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE taxe_sejour (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, prix_enfant INTEGER NOT NULL, prix_adulte INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, profil_id INTEGER NOT NULL, username VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F85E0677 ON users (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9275ED078 ON users (profil_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE camping');
        $this->addSql('DROP TABLE factures');
        $this->addSql('DROP TABLE ligne_facture');
        $this->addSql('DROP TABLE planing');
        $this->addSql('DROP TABLE profils');
        $this->addSql('DROP TABLE taxe_piscine');
        $this->addSql('DROP TABLE taxe_sejour');
        $this->addSql('DROP TABLE users');
    }
}

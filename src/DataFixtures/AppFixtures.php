<?php

namespace App\DataFixtures;

use App\Entity\Camping;
use App\Entity\Profils;
use App\Entity\TaxePiscine;
use App\Entity\TaxeSejour;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create("fr_FR");

        // Creation des camping

        // Creation des compte
        $username = ['darkolink', 'yuzuvrali', 'kilian'];


        foreach ($username as $k => $v) {
                $profils = new  Profils();

                $profils->setPrenom($faker->firstName)
                    ->setNom($faker->lastName)
                    ->setPhone($faker->phoneNumber);

                $manager->persist($profils);

                $users = new Users();

                $users->setEmail($faker->email)
                    ->setUsername($v)
                    ->setPassword($this->passwordEncoder->encodePassword($users,'test'))
                    ->setProfil($profils);

                $manager->persist($users);

        }

        for ($i = 0; $i < 90; $i++ ) {
            if ($i < 50) {
                $logement = "mobile_homes";
                $type = 2;
                $image = "mobile-homes-particulier.jpg";
                $taille = rand(3,8);
                if ($i < 30) {
                    $type = 1;
                    $image = "mobile-homes.jpg";
                }
                switch ($taille) {
                    case 3:
                        $prix = 20;
                        break;
                    case 4:
                        $prix = 24;
                        break;
                    case 5:
                        $prix = 27;
                        break;
                    case 6:
                    case 7:
                    case 8:
                        $prix = 34;
                        break;
                }
            }elseif ($i < 60) {
                $logement = "caravan";
                $type = 0;
                $image = "caravan.jpg";
                $taille = rand(2,6);
                switch ($taille) {
                    case 2:
                        $prix = 15;
                        break;
                    case 3:
                        $prix = 18;
                        break;
                    case 4:
                    case 5:
                        $taille = 3;
                        $prix = 18;
                        break;
                    case 6:
                        $prix = 24;
                        break;
                }
            }else {
                $logement = "emplacement";
                $taille = rand(8, 12)."m²";
                $image = "emplacement.jpg";
                switch ($taille) {
                    case 8:
                        $prix = 12;
                        break;
                    case 9:
                    case 10:
                    case 11:
                        $taille = 8 . "m²";
                        $prix = 12;
                        break;
                    case 12:
                        $prix = 14;
                        break;
                }
            }

            $camping = new Camping();
            $camping->setLogement($logement)
                ->setDescription($faker->paragraph)
                ->setTaille($taille)
                ->setType($type)
                ->setPrix($prix)
                ->setImage($image)
                ->setUsers($users);

            $manager->persist($camping);
        }

        //Creation des Taxe
        for ($i = 0; $i < 1; $i++) {
            $sejour = new TaxeSejour();
            $piscine = new TaxePiscine();

            $sejour->setPrixEnfant(0.35 * 100)
                ->setPrixAdulte(0.60 * 100);

            $piscine->setPrixEnfant(1* 100)
                ->setPrixAdulte(1.5 * 100);

            $manager->persist($sejour);
            $manager->persist($piscine);
        }



        $manager->flush();
    }
}

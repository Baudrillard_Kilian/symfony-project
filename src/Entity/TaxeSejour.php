<?php

namespace App\Entity;

use App\Repository\TaxeSejourRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TaxeSejourRepository::class)
 */
class TaxeSejour
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixEnfant;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixAdulte;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrixEnfant(): ?int
    {
        return $this->prixEnfant;
    }

    public function setPrixEnfant(int $prixEnfant): self
    {
        $this->prixEnfant = $prixEnfant;

        return $this;
    }

    public function getPrixAdulte(): ?int
    {
        return $this->prixAdulte;
    }

    public function setPrixAdulte(int $prixAdulte): self
    {
        $this->prixAdulte = $prixAdulte;

        return $this;
    }
}

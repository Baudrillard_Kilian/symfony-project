<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscriptionUserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                "attr" => ['class' => "form-control"]
            ])
            ->add('username', null,[
                "attr" => ['class' => "form-control"]
            ])
            ->add('password', PasswordType::class,[
                "attr" => ['class' => "form-control"]
            ])
            ->add('profil', InscriptionProfilType::class, [
                "attr" => ['class' => "form-control"]
            ])
            ->add( 'button', SubmitType::class, [
                'label' => "S'inscrire",
                "attr" => ['class' => "btn btn-default btn-outline-primary btn-sm"]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}

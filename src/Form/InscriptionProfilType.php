<?php

namespace App\Form;

use App\Entity\Profils;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscriptionProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prenom', null, [
                "attr" => ['class' => "form-control"]
            ])
            ->add('nom', null, [
                "attr" => ['class' => "form-control"]
            ])
            ->add('phone', TelType::class, [
                "attr" => ['class' => "form-control"]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Profils::class,
        ]);
    }
}

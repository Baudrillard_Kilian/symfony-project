<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Require ROLE_ADMIN for only this controller method.
 *
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{

    /**
     * Require ROLE_ADMIN for only this controller method
     *
     * @IsGranted("ROLE_ADMIN")
     */
    public function adminDashboard()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        // or add an optional message - seen by developers
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Ceci est une page admin vous ne pouvais pas y acceder');
    }

    /**
     * @Route("/admin", name="pageAdmin", methods={"GET"})
     */
    public function pageAdmin()
    {
        return $this->render("admin/pageAdmin.html.twig");
    }

    /**
     * @Route("/tarif", name="grilleTarif", methods={"GET"})
     */
    public function pageGrilleTarif()
    {
        return $this->render("admin/grilleTarif.html.twig");
    }
}
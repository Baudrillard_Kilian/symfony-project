<?php


namespace App\Controller;


use App\Entity\Profils;
use App\Entity\Users;
use App\Form\InscriptionProfilType;
use App\Form\InscriptionUserType;
use App\Repository\CampingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class HomeController extends AbstractController
{
    private $repository;
    private $em;

    public function __construct(CampingRepository $campingRepository, EntityManagerInterface $em)
    {
        $this->repository = $campingRepository ;
        $this->em = $em;
    }

    /**
     * @Route("/accueil", name="accueil", methods={"GET"})
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function pageAccueil(PaginatorInterface $paginator, Request $request)
    {
        $camping = $paginator->paginate(
            $this->repository->findAllAnnonce(),
            $request->query->getInt('page', 1), 4);

        return $this->render("front/accueil.html.twig", [
            "camping" => $camping,
        ]);
    }


    /**
     * @Route("/inscription", name="inscription", methods={"GET", "POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function pageInscription(Request $request)
    {
        $u = new Users();

        $newUsers = $this->createForm(InscriptionUserType::class, $u);

        $newUsers->handleRequest($request);
        if($newUsers->isSubmitted()){
            $this->em->persist($newUsers->getData());
            $this->em->flush();

        }

        return $this->render("front/login-sign_up.html.twig", [
            'title' => 'Inscription',
            "users" => $newUsers->createView(),
            'page' => false,
        ]);
    }

    /**
     * @Route("/annonceDetail/{idCamping}", name="annonceDetail", methods={"GET"})
     * @param string $idCamping
     * @return Response
     */
    public function annonceDetailler(string $idCamping)
    {
        $camping = $this->repository->find($idCamping);
        return $this->render("front/detail.html.twig", [
            "detailCamping" => $idCamping,
            "camping" => $camping,
        ]);
    }

    /**
     * @Route("/reservation/{id}", name="reservation", methods={"GET","POST"})
     */
    public function pageReservation()
    {
        return $this->render("front/reservation.html.twig");
    }

    /**
     * @Route("/location", name="pagePropitaire", methods={"GET", "POST"})
     */
    public function pagePropietaire()
    {
        return $this->render("front/reservation.html.twig");
    }
}
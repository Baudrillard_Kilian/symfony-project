<?php

namespace App\Repository;

use App\Entity\TaxePiscine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TaxePiscine|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaxePiscine|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaxePiscine[]    findAll()
 * @method TaxePiscine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaxePiscineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaxePiscine::class);
    }

    // /**
    //  * @return TaxePiscine[] Returns an array of TaxePiscine objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaxePiscine
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
